<?php

namespace App\Http\Controllers;

// use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\Echo_;

class LokasiController extends Controller
{
    var $url = 'https://dev.farizdotid.com/api/daerahindonesia/';

    public function provinsi()
    {
        $response = file_get_contents($this->url . 'provinsi');
        $data = json_decode($response, true);

        return response()->json([
            'status' => 'SUCCESS',
            'message' => 'List Semua Provinsi Berhasil Dimuat',
            'data'    => $data
        ], 200);
    }

    public function provinsidetail($id)
    {
        $response = @file_get_contents($this->url . 'provinsi/' . $id);
        $data = json_decode($response, true);

        if ($response === FALSE) {
            return response()->json([
                'status' => 'FAILED',
                'message' => 'Data Tidak Ditemukan!',
            ], 404);
        } else {
            return response()->json([
                'status'   => 'SUCCESS',
                'message'   => 'Detail Provinsi Berhasil Dimuat!',
                'data'      => $data
            ], 200);
        }
    }

    public function kabupaten($id)
    {

        $response = @file_get_contents($this->url . '/kota?id_provinsi=' . $id);
        $data = json_decode($response, true);

        if ($response === FALSE) {
            return response()->json([
                'status' => 'FAILED',
                'message' => 'Data Tidak Ditemukan!',
            ], 404);
        } else {
            return response()->json([
                'status'   => 'SUCCESS',
                'message'   => 'Data Kabupaten Berhasil Dimuat!',
                'data'      => $data
            ], 200);
        }
    }

    public function kabupatendetail($id)
    {

        $response = @file_get_contents($this->url . '/kota/' . $id);
        $data = json_decode($response, true);

        if ($response === FALSE) {
            return response()->json([
                'status' => 'FAILED',
                'message' => 'Data Tidak Ditemukan!',
            ], 404);
        } else {
            return response()->json([
                'status'   => 'SUCCESS',
                'message'   => 'Detail Kabupaten Berhasil Dimuat!',
                'data'      => $data
            ], 200);
        }
    }


    public function kecamatan($id)
    {

        $response = @file_get_contents($this->url . '/kecamatan?id_kota=' . $id);
        $data = json_decode($response, true);

        if ($response === FALSE) {
            return response()->json([
                'status' => 'FAILED',
                'message' => 'Data Tidak Ditemukan!',
            ], 404);
        } else {
            return response()->json([
                'status'   => 'SUCCESS',
                'message'   => 'Data Kecamatan Berhasil Dimuat!',
                'data'      => $data
            ], 200);
        }
    }

    public function kecamatandetail($id)
    {

        $response = @file_get_contents($this->url . '/kecamatan/' . $id);
        $data = json_decode($response, true);

        if ($response === FALSE) {
            return response()->json([
                'status' => 'FAILED',
                'message' => 'Data Tidak Ditemukan!',
            ], 404);
        } else {
            return response()->json([
                'status'   => 'SUCCESS',
                'message'   => 'Detail Kecamatan Berhasil Dimuat!',
                'data'      => $data
            ], 200);
        }
    }

    public function kelurahan($id)
    {

        $response = @file_get_contents($this->url . '/kelurahan?id_kecamatan=' . $id);
        $data = json_decode($response, true);

        if ($response === FALSE) {
            return response()->json([
                'status' => 'FAILED',
                'message' => 'Data Tidak Ditemukan!',
            ], 404);
        } else {
            return response()->json([
                'status'   => 'SUCCESS',
                'message'   => 'Data Kelurahan Berhasil Dimuat!',
                'data'      => $data
            ], 200);
        }
    }

    public function kelurahandetail($id)
    {

        $response = @file_get_contents($this->url . '/kelurahan/' . $id);
        $data = json_decode($response, true);

        if ($response === FALSE) {
            return response()->json([
                'status' => 'FAILED',
                'message' => 'Data Tidak Ditemukan!',
            ], 404);
        } else {
            return response()->json([
                'status'   => 'SUCCESS',
                'message'   => 'Detail Kelurahan Berhasil Dimuat!',
                'data'      => $data
            ], 200);
        }
    }

    public function notfound()
    {
        return response()->json([
            'status' => 'FAILED',
            'message' => 'Halaman yang Anda Minta Tidak Ditemukan!',
        ], 404);
    }
}
