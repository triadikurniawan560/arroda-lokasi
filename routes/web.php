<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
$router->group(['prefix' => 'v1/lokasi'], function () use ($router) {
    $router->get('/provinsi', 'LokasiController@provinsi');

    $router->get('/provinsidetail/{id}', 'LokasiController@provinsidetail');

    $router->get('/kabupaten/{id}', 'LokasiController@kabupaten');

    $router->get('/kabupatendetail/{id}', 'LokasiController@kabupatendetail');

    $router->get('/kecamatan/{id}', 'LokasiController@kecamatan');

    $router->get('/kecamatandetail/{id}', 'LokasiController@kecamatandetail');

    $router->get('/kelurahan/{id}', 'LokasiController@kelurahan');

    $router->get('/kelurahandetail/{id}', 'LokasiController@kelurahandetail');
});


// $router->get('/{any:.*}', 'LokasiController@notfound');
// $router->addRoute(['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'], '*', 'LokasiController@notfound');
